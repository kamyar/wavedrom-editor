Wavedrom Editor NWJS Application
=======

Sanitized fork of [http://wavedrom.com/editor.html](http://wavedrom.com/editor.html)

## Build

``npm install -g nw-builder``

``nwbuild -p <OS> .``

To build on macOS:

``nwbuild -p osx64 .``
